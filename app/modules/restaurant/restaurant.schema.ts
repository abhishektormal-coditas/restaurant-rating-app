import { IRating } from "./restaurant.types";


class RestaurantSchema {
    ratings: IRating[] = [{
        name: 'Abhishek',
        rating: [{
            nameOfRestaurant: 'Blue Leaf',
            Food: 4,
            Ambiance: 3,
            Service: 4,
            Cleanlines: 4,
            Overall: 5
        }]
    }];

    create(rating: IRating) {
        this.ratings.push(rating);

        return true;
    }

    get() {
        return this.ratings;
    }

    getOne(name: string) {
        const rating = this.ratings.find(x => x.name === name);

        return rating;
    }

    getOverallAverage(nameOfRestaurant: string) {
        const averageRating = this.ratings.find()
        return averageRating
    }
}

const restaurantSchema = new RestaurantSchema();

export default restaurantSchema;