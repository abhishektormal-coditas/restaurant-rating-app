type ratingType = {
    nameOfRestaurant: string,
    Food: number,
    Ambiance: number,
    Service: number,
    Cleanlines: number,
    Overall: number
};

export interface IRating {
    name: string;
    rating: ratingType[];
}

export class RestaurantResponse {
    constructor(
        public statusCode: number,
        public message: string
    ) { }
}

