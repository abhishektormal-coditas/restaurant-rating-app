import { RATING_MESSAGE } from "./restaurant.constants";
import restaurantSchema from "./restaurant.schema";
import { IRating } from "./restaurant.types";


const create = (rating: IRating) => restaurantSchema.create(rating);

const getAll = (role: string) => {
    if(role === 'manager' || role === 'admin') {
        return restaurantSchema.get();
    }
    throw(RATING_MESSAGE.UNAUTHORIZED);
}

const getOne = (name: string, role: string) => {
    if(role === 'manager' || role === 'admin') {
        const rating = restaurantSchema.getOne(name);
        if(rating) {
            return rating;
        }
        throw RATING_MESSAGE.NOT_FOUND;
    }
    throw(RATING_MESSAGE.UNAUTHORIZED);
    
}

const getOverallAverage = (nameOfRestaurant: string, role: string) {
    if(role === 'admin') {
        const averageRating = restaurantSchema.getOverallAverage();
        if(averageRating) {
            return averageRating;
        }
        throw RATING_MESSAGE.NOT_FOUND;
    }
    throw(RATING_MESSAGE.UNAUTHORIZED);
}


export default {
    create,
    getAll,
    getOne
}
