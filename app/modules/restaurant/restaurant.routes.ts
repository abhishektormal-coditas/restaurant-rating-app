import { Router } from 'express';
import { Response } from '../../utility/response-handler';
import { RATING_MESSAGE } from './restaurant.constants';
import restaurantService from './restaurant.service';
import { IRating } from './restaurant.types';


const router = Router();

router.post("/", (req, res, next) => {
    restaurantService.create(req.body as IRating);
    next(new Response(RATING_MESSAGE.SUBMITTED));
});

router.get("/", (req, res, next) => {
    try {
        const role = req.headers.role as string ;
        const result = restaurantService.getAll(role); 
        next(new Response(result)); 
    } catch (e) {
        next(new Response(e));
    }
    
});

router.get("/:name", (req, res, next) => {
    try {
        const role = req.headers.role as string ;
        const result = restaurantService.getOne(req.params.name, role);
        next(new Response(result));
    } catch (e) {
        next(e);
    }
})

router.get("/:nameOfRestaurant", (req, res, next) => {
    try {
        const role = req.headers.role as string ;
        const result = restaurantService.getOverallAverage(req.params.nameOfRestaurant, role);
        next(new Response(result));
    } catch (e) {
        next(e);
    }
})


export default router;
