import { RestaurantResponse } from "./restaurant.types";


export const RATING_MESSAGE = {
    NOT_FOUND: new RestaurantResponse(404, "RATING NOT FOUND"),
    UNAUTHORIZED: new RestaurantResponse(401, "UNAUTHORIZED"),
    SUBMITTED: new RestaurantResponse(201, "RATING IS SUBMITTED")
}
