import RestaurantRouter from '../restaurant/restaurant.routes'
import { Route, Routes } from './routes.type';

export const routes: Routes = [
    new Route("/restaurants", RestaurantRouter)
]